<?php

namespace App\Helper;

use Core\Container;

class FetchUserPosts
{

    use CurlRequest;

    private $config;

    public function __construct()
    {
        $this->config = Container::get('config');
    }

    public function getAllUserPostsData():array
    {
        $all_posts = [];
        try {
            $sl_token = $this->getToken();
        } catch (\Exception $e) {
            $sl_token = null;
        }
        if ($sl_token) {
            for ($i = 1; $i <= 10; $i++) {
                $requestData = [
                    'url'    => $this->config['user_posts']['url'],
                    'params' => [
                        'sl_token' => $sl_token,
                        'page'     => $i
                    ]
                ];
                $response = $this->getRequest($requestData);
                try {
                    $posts = json_decode($response, true)['data']['posts'];
                } catch (\Exception $e) {
                    $posts = [];
                }
                $all_posts[] = $posts;
            }
            return array_merge([], ...$all_posts);
        }
        throw new \Exception('Unable to get Short lived sl_token');
    }

    private function getToken()
    {
        $response = $this->registerForSlToken();
        try {
            return json_decode($response, true)['data']['sl_token'];
        } catch (\Exception $e) {
            throw new \Exception('Invalid json format');
        }
    }

    private function registerForSlToken()
    {
        return $this->postRequest($this->config['register']);
    }

}

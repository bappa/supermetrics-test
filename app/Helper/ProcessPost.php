<?php

namespace App\Helper;

class ProcessPost
{

    /**
     * @var
     */
    protected $posts;

    /**
     * ProcessPost constructor.
     *
     * @param $posts
     */
    public function __construct($posts)
    {
        $this->posts = $posts;
    }

    /**
     * @return array
     */
    public function avgCharLengthPerMonth():array
    {
        $result = [];
        $posts = $this->processPostsByDate();
        foreach ($posts as $key => $post) {
            $monthly_post = count($post);
            $sum = 0;
            foreach ($post as $item) {
                $sum += $item['char_len'];
            }
            $result[$key] = floatValue($sum / $monthly_post);
        }
        return $result;
    }

    /**
     * @param string $groupBy
     * @return array
     */
    protected function processPostsByDate($groupBy = 'created_time'):array
    {
        foreach ($this->posts as $key => $post) {
            $this->posts[$key]['created_time'] = date('Y-m', strtotime($post['created_time']));
            $this->posts[$key]['created_week'] = date('W', strtotime($post['created_time']));
            $this->posts[$key]['char_len'] = strlen($post['message']);
        }
        $column = array_column($this->posts, 'created_time');
        array_multisort($column, SORT_ASC, $this->posts);
        return groupByKey($groupBy, $this->posts);
    }

    /**
     * @return array
     */
    public function longestPostByCharLengthPerMonth():array
    {
        $result = [];
        $posts = $this->processPostsByDate();
        foreach ($posts as $key => $post) {
            $result[$key] = max(array_column($post, 'char_len'));
        }
        return $result;
    }

    public function totalPostsSplitByWeekNum():array
    {
        $result = [];
        $postByWeek = $this->processPostsByDate('created_week');
        $totalPosts = 0;
        foreach ($postByWeek as $key => $post) {
            $totalPosts += count($post);
        }
        $result[] = floatValue($totalPosts/count($postByWeek));
        return $result;
    }

    /**
     * @return array
     */
    public function avgNumOfPostPerUserPerMonth():array
    {
        $result = [];
        $posts = $this->processPostsByUser('name');
        foreach ($posts as $key => $post) {
            $no_of_month = count(array_unique(array_column($post, 'created_time')));
            $result[$key] = floatValue(count($post) / $no_of_month);
        }
        return $result;
    }

    /**
     * @param string $groupBy
     * @return array
     */
    protected function processPostsByUser($groupBy = 'created_time'):array
    {
        foreach ($this->posts as $key => $post) {
            $this->posts[$key]['name'] = $post['from_name'] . '(' . $post['from_id'] . ')';
        }
        $column = array_column($this->posts, 'name');
        array_multisort($column, SORT_ASC, $this->posts);
        return groupByKey($groupBy, $this->posts);
    }

    /**
     * @return array
     */
    private function getUsers():array
    {
        $users = array_values(array_unique(array_column($this->posts, 'from_id')));
        sort($users);
        return $users;
    }

}

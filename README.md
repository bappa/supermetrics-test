##Supermetrics Assignment

---
###Author: Iftakharul Alam
> Project Requirement

|Requirement| |
|---|---|
|php| >= 7|
|ext-curl|Yes|
|ext-json|Yes|
|ext-cli|Yes|

> Project setup
```shell
git clone https://gitlab.com/bappa/supermetrics-test.git
cd supermetrics-test
composer dumpautoload
php -S localhost:3000
```
Now go to browser and hit

**http://localhost:3000**

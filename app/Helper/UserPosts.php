<?php

namespace App\Helper;

class UserPosts
{

    public function getUserPostsStatistics():array
    {
        $allPosts = $_SESSION['posts']??null;
        if (!$allPosts) {
            $fetchUserPosts = new FetchUserPosts();
            $allPosts = $fetchUserPosts->getAllUserPostsData();
            $_SESSION['posts'] = $allPosts;
        }
        $processPostsData = new ProcessPost($allPosts);
        return [
            "Average Character Length Per Month"         => $processPostsData->avgCharLengthPerMonth(),
            "Longest Post By Character Length Per Month" => $processPostsData->longestPostByCharLengthPerMonth(),
            "Total post split by week num"               => $processPostsData->totalPostsSplitByWeekNum(),
            "Average Number of Post Per User Per Month"  => $processPostsData->avgNumOfPostPerUserPerMonth()
        ];
    }

}

<?php

namespace Core;

class Router
{

    /**
     * All registered routes.
     *
     * @var array
     */
    public $routes = [
        'GET'    => [],
        'POST'   => [],
        'PUT'    => [],
        'PATCH'  => [],
        'DELETE' => []
    ];

    /**
     * Load a user's routes file.
     *
     * @param string $file
     * @return Router
     */
    public static function load($file):Router
    {
        $route = new static();
        require $file;
        return $route;
    }

    /**
     * Register a GET route.
     *
     * @param string $uri
     * @param array $controller
     */
    public function get(string $uri, array $controller)
    {
        $this->routes['GET'][$uri] = $controller;
    }

    /**
     * Load the requested URI's associated controller method.
     *
     * @param string $uri
     * @param string $requestType
     * @return mixed
     */
    public function process(string $uri, string $requestType)
    {
        if (array_key_exists($uri, $this->routes[$requestType])) {
            return $this->callAction($this->routes[$requestType][$uri]);
        }
        throw new \RuntimeException('No route defined for this URI.');
    }

    /**
     * Load and call the relevant controller action.
     *
     * @param array $controllerAction
     * @return mixed
     */
    protected function callAction(array $controllerAction)
    {
        [$controller, $action] = $controllerAction;
        $controller = new $controller();
        if (!method_exists($controller, $action)) {
            throw new \RuntimeException("Controller method doesn't exist");
        }
        return $controller->$action();
    }

}

<?php

use Core\Request;
use Core\Router;

require 'vendor/autoload.php';
require 'core/bootstrap.php';
Router::load('routes.php')->process(Request::uri(), Request::method());

<?php
if (!function_exists('response')) {
    /**
     * Json response.
     *
     * @param array $data
     */
    function response($data = [])
    {
        header('Content-Type: application/json');
        $time = number_format(microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"], 4) . ' s';
        $result = ["Execution Time" => $time, "Statistics" => $data];
        return print json_encode($result);
    }
}
if (!function_exists('groupByKey')) {
    /**
     * Array group by key
     *
     * @param $key
     * @param $data
     * @return array
     */
    function groupByKey($key, $data):array
    {
        $result = [];
        foreach ($data as $val) {
            if (array_key_exists($key, $val)) {
                $result[$val[$key]][] = $val;
            } else {
                $result[""][] = $val;
            }
        }
        return $result;
    }
}
if (!function_exists('floatValue')) {
    /**
     * Custom function for float number representation
     *
     * @param $number
     * @param int $decimal
     * @return float
     */
    function floatValue($number, $decimal = 1):float
    {
        return (float)number_format($number, $decimal);
    }
}

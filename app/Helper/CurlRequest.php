<?php

namespace App\Helper;

trait CurlRequest
{

    private function postRequest($requestData)
    {
        $cURLConnection = curl_init($requestData['url']);
        curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, $requestData['params']);
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
        $apiResponse = curl_exec($cURLConnection);
        curl_close($cURLConnection);
        return $apiResponse;
    }

    private function getRequest($requestData)
    {
        $cURLConnection = curl_init($requestData['url'] . '?' . http_build_query($requestData['params']));
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
        $apiResponse = curl_exec($cURLConnection);
        curl_close($cURLConnection);
        return $apiResponse;
    }

}

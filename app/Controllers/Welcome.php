<?php

namespace App\Controllers;

use App\Helper\UserPosts;
use Exception;

class Welcome
{

    /**
     * All the processed post.
     */
    public function index()
    {
        try {
            $processedPost = new UserPosts();
            return response($processedPost->getUserPostsStatistics());
        } catch (Exception $exception) {
            return response($exception->getMessage());
        }
    }

}
